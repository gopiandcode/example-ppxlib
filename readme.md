# Custom PPXlib notation for Pairs

To build run from the project root:

```
    dune build
```

To execute the program, run the following from the project root:

```
    dune exec ./notation.exe
```
