open Ppxlib

let name = "mp"

let expand ~loc ~path:_ expr =
  match expr with
  | {pexp_desc = Pexp_sequence (e1,e2); _} -> [%expr Pair.MkPair ([%e e1], Some [%e e2]) ]
  | e1 -> [%expr Pair.MkPair ([%e e1], None) ]



let ext =
  Extension.declare name Extension.Context.expression
    Ast_pattern.(single_expr_payload __)
    expand


let () = Driver.register_transformation name ~extensions:[ext]
