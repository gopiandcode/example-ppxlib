type 'a maybe_pair = MkPair of 'a * 'a option [@@deriving show]
