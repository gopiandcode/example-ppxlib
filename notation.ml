
let () =
  let x = [%mp 1] in
  Format.printf "x is %a\n" (Pair.pp_maybe_pair Format.pp_print_int) x;
  let x = [%mp 1; 2] in    
  Format.printf "x is %a\n" (Pair.pp_maybe_pair Format.pp_print_int) x

